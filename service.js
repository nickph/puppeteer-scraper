import puppeteer from 'puppeteer'

/**
 * Get info for all NFT assets
 * @param {{assetContractAddress: string, tokenId: number}[]} tokenList
 * @returns { {date, isActive, currentPrice, currentFiatPrice}[] }
 */
export default async function getAssetsInfo (tokenList) {
  const BASE_URL = 'https://opensea.io/assets/matic'

  const browser = await puppeteer.launch({
    devtools: false
  })

  /**
   * Scrape one page
   * @param {string} assetContractAddress
   * @param {number} tokenId
   */
  async function scrapePage (assetContractAddress, tokenId) {
    const url = `${BASE_URL}/${assetContractAddress}/${tokenId}`
    console.log('Scraping page: ', url)
    //console.log('Starting a new page')
    const page = await browser.newPage()
    //console.log('Setting user agent')
    await page.setUserAgent(
      'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4691.0 Safari/537.36'
    )

    // make the page skip requests for images and styles
    // TODO: still loading stylesheet
    await page.setRequestInterception(true)
    page.on('request', req => {
      if (
        req.resourceType() === 'image' ||
        req.resourceType() === 'stylesheet'
      ) {
        req.abort()
      } else {
        req.continue()
      }
    })

    // record console message from the page into this terminal
    // page.on('console', msg => console.log('PAGE LOG:', msg.text()))
    await page.goto(url)

    // take a screenshot of the page (to see what headless see)
    // await page.screenshot({ path: 'example.png' })
    await page.waitForSelector('.item--frame')

    console.log('evaluating')
    const assetData = await page.evaluate(
      (assetContractAddress, tokenId) => {
        const itemFrame = document.querySelectorAll('.item--frame')[1]
        let currentPrice = null
        let currentFiatPrice = null
        const date =
          itemFrame
            .querySelector('.TradeStation--header')
            ?.querySelectorAll('span')[1].innerText ?? null
        const isActive = !!date

        if (isActive) {
          currentPrice = itemFrame.querySelector('.Price--amount').innerText
          currentFiatPrice = itemFrame.querySelector('.Price--fiat-amount')
            .innerText
        }

        return {
          assetContractAddress,
          tokenId,
          isActive,
          date,
          currentPrice,
          currentFiatPrice
        }
      },
      assetContractAddress,
      tokenId
    )

    console.log(assetData)
    await page.close()
    return assetData
  }

  try {
    const assetInfo = await Promise.allSettled(
      tokenList.map(el => scrapePage(el.assetContractAddress, el.tokenId))
    )

    return assetInfo
  } catch (err) {
    console.error(err)
  } finally {
    browser.close()
  }
}

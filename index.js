import express from 'express'
import getAssetsInfo from './service.js'

const app = express()
const port = 5000

app.get('/api', async (req, res) => {
  const { tokenList } = req.query

  let start, end
  start = new Date()
  const assetInfo = await testServiceQuery(tokenList)
  end = new Date()
  
  console.log(`Operation completed in ${end.getTime() - start.getTime()} ms`)
  res.json({ result: assetInfo })
})

app.listen(port, () => {
  console.log(`Service listening on port http://localhost:${port}`)
})